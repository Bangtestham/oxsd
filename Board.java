
public class Board {
	private char [] [] table = {
            {'-', '-', '-'},
            {'-', '-', '-'},
            {'-' ,'-' ,'-'},
    };

	private	Player o;
	private Player x;
	private	Player win;
	private Player currentplayer;
	private int turnCount;
	
	public Board(Player x, Player o) {
		this.x = x;
		this.o = o;
		currentplayer = x;
			win = null;
		turnCount = 0;
		
	}
	
	public char[][] getTable(){
		return table;
	}

	
	public Player getCurrentplayer() {
		return currentplayer;
	}

	public boolean setTable(int row,int col) {
		if(table[row][col]=='-') {
			table[row][col] = currentplayer.getName();
			return true;
		}
		
		return false;
		
	}
	private boolean checkRow(int row) {
		for(int col=0; col<table[row].length; col++) {
			if(table[row][col]!= currentplayer.getName()) {
				return false;
			}
		}
		return true;
	}
	private boolean checkRow() {
		if(checkRow(0)|| checkRow(1)|| checkRow(2)) {
			return true;
		}
		return false;
		
	}
	private boolean checkCol(int col) {
		for(int row=0; row<table.length; row++) {
			if(table[row][col]!= currentplayer.getName()) {
				return false;
			}
		}
		return true;
	}
	private boolean checkCol() {
		if(checkCol(0)|| checkCol(1)|| checkCol(2)) {
			return true;
		}
		return false;
		
	}
	
	
	private boolean checkX1() {
		for(int i=0; i<table.length; i++) {
			if(table[i][i]!= currentplayer.getName()) {
				return false;
			}
		}
		return true;
	}
	
	private boolean checkX2() {
		for(int i=0; i<table.length; i++) {
			if(table[2-i][i]!= currentplayer.getName()) {
				return false;
			}
		}
		return true;
	}
	private boolean checkDarw() {
		if(turnCount==8) {
			x.draw();
			o.draw();
			return true;
		}
		return false;
	}
	public boolean checkWin() {
		if(checkRow()|| checkCol() || checkX1() || checkX2()) {
			win = currentplayer;
			if(currentplayer==x) {
				x.win();
				o.lose();
			}else {
				o.win();
				x.lose();
			}
			return true;
	}
		return false;
}
	public Player getWin() {
		return win;
	}
	public boolean isFinish() {
		if(checkWin()) {
			return true;
		}
		if(checkDarw()) { 
			return true;
		}
		
		return false;
	}
	
	public void switchPlayer() {
		if(currentplayer==x) {
			currentplayer = o;
			
		}else {
			currentplayer =x ;
		}
		turnCount++;
	}
	
}

