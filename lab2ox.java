import java.util.Scanner;
	
public class lab2ox {
	static char [] [] table = {
            {'-', '-', '-'},
            {'-', '-', '-'},
            {'-' ,'-' ,'-'},
    };
	static int row,col;
	static char player = 'X';
	static int trun = 0;
	public static void main(String[] args) {
		showWelcome();
		while(true) {
			showTable(); // table
			showTurn();
			input(); // row col,player
			if(checkWin()) { //true
				break;
			}
			switchPlayer();
		}
		showTable();
		showWin();
		showBye();
		
	}
	private static void switchPlayer() {
		if(player =='X') {
			player = 'O';
		}else {
			player ='X';
		}
		trun++;
	}
	
	
	private static void showBye() {
		System.out.println("Bye Bye....");
	}
	
	private static void input() {
		while (true) {
			try {
				Scanner kb = new Scanner(System.in);
				System.out.print("please input Row Col: " );
			
				String input = kb.nextLine();
				String str[] = input.split(  " ");
				if(str.length !=2) {
					System.out.println("Error: please input Row Col (Ex. 1 1)");
					continue;
				}
				 row = Integer.parseInt(str[0]);
				 col = Integer.parseInt(str[1]);
				 if(row>3||row<1 || col>3 || col<1) {
					 System.out.println("Error: please input Row Col between 1-3");
					 continue;
				 }
				if(!setTable()){
				System.out.println("Error: please another Row Col");
				continue;
			}
				break;
		}catch(Exception e) {
				System.out.println("Error: please input Row Col (Ex. 1 1");
				continue;
			}
			
		}
		
	}
	private static boolean setTable() { //Error false
		if(table[row-1][col-1] !='-') {
		return false;
	}
	table[row-1][col-1] = player;
	return true;
	}
	private static void showTurn() {
		System.out.println(player + " turn ");
	}
	
	private static void showTable() {
		System.out.println("  1 2 3");
		for(int rowIndex = 0; rowIndex<table.length; rowIndex++) {
			System.out.print(rowIndex+1);
			for(int colIndex = 0; colIndex<table[rowIndex].length; colIndex++) {
				
				System.out.print(" " + table[rowIndex][colIndex]);
			}
			System.out.println();
		}
	}
	
	private static void showWelcome() {
		System.out.println("Welcome to OX Game");
	}
	
	private static boolean checkWin() {
		if(checkRow()) {
			return true;
		}
		if(checkCol()) {
			return true;
		}
		if(checkX()) {
			return true;
		}
		if(isDraw()) {
			return true;
		}
		return false;
	}
	private static boolean isDraw() {
		if(trun==8) return true;
		return false;
	}
	private static boolean checkX() {
		if(checkX1()) {
			return true;
			}
		if(checkX2()) {
			return true;
			}
		return false;
	}
	private static boolean checkX1() {
		for(int i=0; i< table.length; i++) {
			if(table[i][i]!=player) 
				return false;
		}
		return true;
	}
	private static boolean checkX2() {
		for(int i=0; i< table.length; i++) {
			if(table[i][2-i]!=player) 
				return false;
		}
		return true;
	}
	private static boolean checkRow(int rowIndex) {
		for(int colIndex = 0; colIndex<table[rowIndex].length; colIndex++) {
			if(table[rowIndex][colIndex]!= player) 
				return false;
		}
		return true;
		
	}
	private static boolean checkRow() {
		for(int rowIndex = 0; rowIndex< table.length; rowIndex++) {
			if(checkRow(rowIndex)) 
				return true;
		}
		return false;
		
	}
	
	private static boolean checkCol(int colIndex) {
		for(int rowIndex = 0; rowIndex<table[colIndex].length; rowIndex++) {
			if(table[rowIndex][colIndex]!= player) 
				return false;
		}
		return true;
		
	}
	private static boolean checkCol() {
		for(int colIndex = 0; colIndex< table[0].length; colIndex++) {
			if(checkCol(colIndex)) 
				return true;
		}
		return false;
		
	}
	private static void showWin() {
		if(isDraw()) {
			System.out.println("Draw");
			
		}else {
			System.out.println( player + " Win....");
		}
	}
}


























