import java.util.Scanner;

public class Game {
	private Board board;
	private Player x;
	private Player o;
	
	public Game() {
	o = new Player('O');
	x = new Player('X');
	
	}
	
	public void play() {
		while (true) {
			playOne();
		}
	}
	
	public void playOne() {
		board = new Board(x,o);
		showWelcome();
		while(true) {
			showTable();
			showTurn();
			input();
			if(board.isFinish()) {
				break;
				
			}
			board.switchPlayer();
		}
		showTable();
		showWin();
		showStat();
	}
	private void showStat() {
		
		System.out.println(x.getName() +"  Win,Draw,Lose = "
		+ x.getWin() + "," 
		+ x.getDraw() + ","
		+ x.getLose() 
	);
		
		System.out.println(o.getName() +"  Win,Draw,Lose = "
				+ o.getWin() + "," 
				+ o.getDraw() + ","
				+ o.getLose() 
			);
	}
	private void showWin() {
		Player player = board.getWin();
		System.out.println(player.getName()+ " win...");
		
	}
	private void showWelcome() {
		System.out.println("Welcome to ox Game");
	}
	
	private void showTable() {
		char [] [] table = board.getTable();
		System.out.println("  1 2 3");
		for(int i=0; i<table.length; i++) {
			System.out.print((i+1));
			for(int j=0; j<table[i].length; j++) {
			System.out.print(" " + table[i][j]);
		}
			System.out.println();
	}
}	
	private void showTurn() {
		Player player = board.getCurrentplayer();
		System.out.println(player.getName()+" turn..");
	}
	private void input() {
		Scanner kb = new Scanner (System.in);
		while (true) {
			try {
				System.out.print("please input Row Col: ");
				String input = kb.nextLine();
				String[] str = input.split( " ");
				if(str.length !=2) {
					System.out.println("please input Rol Col[1-3] (EX:1 2)");
					continue;
				}
				int row = Integer.parseInt(str[0])-1;
				int col = Integer.parseInt(str[1])-1;
				if(board.setTable(row,col) == false ) {
					System.out.println("Table is not emptry!!");
					continue;
				}
				break;
			}catch(Exception e){
				System.out.println("please input Rol Col[1-3] (EX:1 2)");
				continue;
			}
			
		}
		
	}
	
	
	
}